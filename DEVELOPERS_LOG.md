# CSS: margin-top vs margin-bottom

TLDR: Always use top margin. When you have more complicated layout and you need to edit margings between elements then we can override styles that follow particular elements.

The flexibility of Cascading Style Sheets (CSS) lies in its ability to adjust CSS rules based on preceding elements, as the space between elements is considered part of the element that follows it.

>Link to the article: https://matthewjamestaylor.com/css-margin-top-vs-bottom?utm_content=cmp-true

# Code styling - linting and formatting

What is the difference between linting and formatting?

Lint checks for unintentional mistakes in coding or coding in ways that may lead to incorrect results.

Code formatting is to make it look pretty and easy for ‘someone’ to read.

When collaborating on a shared codebase, it's essential to ensure uniformity and quality across the code. This involves:

>1. code use the same format everywhere (like `tabs` and `spaces`).
>2. code use the best practices for good quality (like `let/const` rather than `var`.)
>
>Code formatter solves the first problem, it reprints the entire program from scratch in a consistent way.
>
>Code linter solves the second problem, it helps to use the better syntaxes or new features of the programming languages and catch possible errors, but it’s not able to solve some difficult ones, like variable naming.

For linting we use ESLint and for formatting Prettier.

>Link to the article: https://medium.com/@awesomecode/format-code-vs-and-lint-code-95613798dcb3

# curl and jq

# curl

# jq

    jq is a lightweight and flexible command-line JSON processor. jq is like sed for JSON data - you can use it to slice and filter and map and transform structured data with the same ease that sed, awk, grep and friends let you play with text.

I have a file data.json:

    {
      "movie": {
        "title": "Harry Potter and the Philosopher's Stone",
        "info": {
          "release_year": 2001,
          "director": "Chris Columbus",
          "writers": ["J.K. Rowling", "Steve Kloves"],
          "runtime_minutes": 152
        },
        "characters": [
          {
            "name": "Harry Potter",
            "wand": "Phoenix Feather",
            "actor": "Daniel Radcliffe"
          },
          {
            "name": "Hermione Granger",
            "wand": "Vine Wood",
            "actor": "Emma Watson"
          },
          {
            "name": "Ron Weasley",
            "wand": "Willow",
            "actor": "Rupert Grint"
          }
        ]
      }
    }

## How to read this file contents and filter out specific data in terminal?

To display file content in terminal:

    ❯ cat data.json

To output the file content as it is and nothing else I can use `jq`.

    ❯ cat data.json | jq
    ⬇️ defaults to:
    ❯ cat data.json | jq '.'

`|` is the pipe operator, which takes the output from one command on its left (`cat data.json`) and passes it as input to the command on its right (`jq '.'`). You're not limited to a single piped command as you can stack them as many times as you like, or until you run out of output or file descriptors.

`jq '.'` is a jq command that performs a simple operation: it takes the input JSON data and outputs it exactly as it is, without any modifications. The dot . in jq represents the root of the JSON document, so jq '.' essentially says "output the entire JSON document as it is."

## What if you want to get writers of the movie?

    ❯ cat data.json | jq '.movie.info.writers'

    [
      "J.K. Rowling",
      "Steve Kloves"
    ]

It filters the input JSON data and extracts the value of the "writers" field, which is nested within the "info" object under the "movie" object.

## What if you want to get only the names of the characters?

    ❯ cat data.json | jq '.movie.characters[].name'

    "Harry Potter"
    "Hermione Granger"
    "Ron Weasley"

It filters the input JSON data and extracts the value of the "name" field from each object within the "characters" array, which is nested within the "movie" object.


# What is symlink?

# What is Camunda and how to use it?

# What is terminal and what is a shell? What is iTerm?

`Terminal` is a program that provides a text-based interface. Terminal is a device that gives you access to the console of your computer. In the past it was a physical device (before terminals were monitors with keyboards, they were teletypes) and then its concept was transferred into software.

`Terminal emulator` is a program that we use these days to access the console, popular ones are gnome-terminal and newer ones are alacritty, I use iTerm2. A Terminal emulator is a program that runs a Shell. So when I open GNOME Terminal, a black window that runs a Shell appears so I can run my commands.

`Shell` is a command-line interpreter that interprets user commands and interacts with the operating system. The shell is the name of the program that runs in the terminal, giving you a command prompt, popular ones are sh, bash, zsh, fish, ash, csh (notice how they all end in -sh?).The Shell is a program which processes commands and returns output, like Bash in Linux. Basically an application for running commands.

`iTerm` is a terminal emulator program specifically designed for macOS, offering enhanced features and customization options compared to the default Terminal application.

                          macOS        
                   (Operating System)        
               _____________|___________ 
              |                         |
           iTerm2               default "Terminal"      
    (Terminal emulator)         (Terminal emulator)      
              |                         |
      "Zsh" - Z shell            default "Bash"       
           (Shell)                   (Shell)    

Bash - "Bourne Again Shell,” developed in 1989.
Terminal - terminal emulator in macOS.

# Helper script for local use

Sometimes you might need quickly something specific in your clipboard. Let it be in this example a test account password. For safety reasons this password must not do any harm when leaked. Otherwise use password manager for this. I saved this to `local.sh` and from there my shell knows to retrieve this. For aliases you can use ``

    #Get a testuser password to clipboard
    testuser() {
      password='th1s1smyPassw0rd'
      printf "%s" "$password" | pbcopy
      echo "Username - user.name"
      echo "Password copied to clipboard."
    }

Run in your shell the command and see results:

    ❯ testuser

    Username - user.name
    Password copied to clipboard.

# How to define aliases and functions to run in your zsh?

You can learn more about Zsh files here: https://zsh.sourceforge.io/Intro/intro_3.html


`~/.zshenv` defines only one variable, ZDOTDIR, because this file is loaded on every shell startup (including `reload`) and thus duplicates may occur, and some variables may contain commands or other dynamic content.
`ZDOTDIR` specifies the location of other configuration files for `Zsh`.
`$ZDOTDIR/.zprofile` contains all environment variables, this file is loaded before `.zshrc` and by that time they are already needed. Additionally, it is only executed in login-type shells, i.e., those where you, as a user, interact. Although Zsh documentation advises against putting variables here, if, for example, PATH is defined there, it will be overwritten later by other files. Furthermore, my research has led me to the conclusion that it is most optimal to keep variables there.
`$ZDOTDIR/.zshrc` sets up Zsh and loads all other scripts needed — aliases, functions, plugins.